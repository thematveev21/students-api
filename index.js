// get
// post
// put
// delete

const express = require('express')
const { MongoClient, ObjectId } = require('mongodb')
const cors = require('cors')
const config = require('./config')


const app = express()
app.use(cors({
    origin: '*'
}))

app.use(express.json())

const client = new MongoClient(
    config.MONGO_CONNECTION_STRING
)
client.connect()

const students = client.db('lms').collection('students')


// add new student
app.post('/api/students', async (req, res) => {
    const data = req.body
    const {insertedId} = await students.insertOne(data)
    res.send({
        status: true,
        id: insertedId
    })
})

// get all students info
app.get('/api/students', async (req, res) => {
    const data = await students.find().toArray()
    res.send(data)
})


// change one student
app.put('/api/students/:id', async (req, res) => {
    const data = req.body
    await students.updateOne({"_id": new ObjectId(req.params.id) }, {"$set": data})
    res.send({status: true})

})

// delete one student
app.delete('/api/students/:id', async (req, res) => {
    await students.deleteOne({"_id": new ObjectId(req.params.id)})
    res.send({status: true})
})


// info about one student
app.get('/api/students/:id', async (req, res) => {
    const data = await students.findOne({"_id": new ObjectId(req.params.id)})
    if (data){
        res.send(data)
    }
    else {
        res.status(404).send({status: false})
    }
})

app.get('/', (req, res) => {
    res.send('OK')
})

// app.listen(process.env.PORT || 3000)

module.exports = app;
